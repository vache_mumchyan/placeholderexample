package com.example.myapplication.data.repository.local

import androidx.lifecycle.LiveData
import com.example.myapplication.data.entity.Comment
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalRepository @Inject constructor(private val dao: CommentDao) {

    fun getAll() = dao.getAll()

    fun insert(comment: Comment) = dao.insert(comment)

    fun delete(comment: Comment) = dao.delete(comment)

}