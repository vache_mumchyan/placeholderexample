package com.example.myapplication.data.repository.local

import androidx.room.*
import com.example.myapplication.data.entity.Comment
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface CommentDao {

    @Query("SELECT * FROM favourite")
    fun getAll(): Single<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comment: Comment): Completable

    @Delete
    fun delete(comment: Comment): Completable

}