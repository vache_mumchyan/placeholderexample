package com.example.myapplication.data.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.data.entity.Comment

@Database(
    entities = [Comment::class],
    version = DemoDatabase.VERSION
)
abstract class DemoDatabase : RoomDatabase() {
    abstract fun commentDao(): CommentDao

    companion object {
        const val VERSION = 1
    }
}