package com.example.myapplication.data.repository.remote

import com.example.myapplication.data.entity.Comment
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteRepository @Inject constructor(private val api: Api) {

     fun getComments(): Single<List<Comment>> = api.getComments()

}