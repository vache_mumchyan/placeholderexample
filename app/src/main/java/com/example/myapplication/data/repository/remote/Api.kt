package com.example.myapplication.data.repository.remote

import com.example.myapplication.data.entity.Comment
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET

interface Api {

    @GET("comments")
    fun getComments(): Single<List<Comment>>

}