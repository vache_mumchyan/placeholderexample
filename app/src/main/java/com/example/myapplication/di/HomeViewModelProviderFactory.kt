package com.example.myapplication.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.data.repository.local.LocalRepository
import com.example.myapplication.data.repository.remote.RemoteRepository
import com.example.myapplication.ui.home.HomeViewModel
import javax.inject.Inject

class HomeViewModelProviderFactory @Inject constructor(
    private val repository: RemoteRepository,
    private val localRepository: LocalRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            HomeViewModel(
                repository,
                localRepository
            ) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}