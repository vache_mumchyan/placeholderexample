package com.example.myapplication.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.data.repository.local.LocalRepository
import com.example.myapplication.ui.favorite.FavoriteViewModel
import javax.inject.Inject

class FavoriteViewModelProviderFactory @Inject constructor(
    private val localRepository: LocalRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(FavoriteViewModel::class.java)) {
            FavoriteViewModel(localRepository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}