package com.example.myapplication.di.component

import android.content.Context
import com.example.myapplication.data.repository.local.LocalRepository
import com.example.myapplication.data.repository.remote.RemoteRepository
import com.example.myapplication.di.module.AppModule
import com.example.myapplication.di.module.NetworkModule
import com.example.myapplication.di.module.RoomModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        RoomModule::class
    ]
)
interface AppComponent {

    fun getContext(): Context

    fun getRemoteRepository(): RemoteRepository

    fun getLocalRepository(): LocalRepository

}