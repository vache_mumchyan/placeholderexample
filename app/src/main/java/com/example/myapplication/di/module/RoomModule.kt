package com.example.myapplication.di.module

import android.content.Context
import androidx.room.Room
import com.example.myapplication.data.repository.local.CommentDao
import com.example.myapplication.data.repository.local.DemoDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Singleton
    @Provides
    fun providesRoomDatabase(context: Context): DemoDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            DemoDatabase::class.java,
            "demo-db"
        ).build()
    }

    @Singleton
    @Provides
    fun providesCommentDao(demoDatabase: DemoDatabase): CommentDao {
        return demoDatabase.commentDao()
    }

}