package com.example.myapplication.di.module

import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.di.HomeViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class MainViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelModelFactory(modelProviderProviderFactoryHome: HomeViewModelProviderFactory)
            : ViewModelProvider.Factory
}