package com.example.myapplication.di.component

import com.example.myapplication.di.module.MainViewModelFactoryModule
import com.example.myapplication.di.scope.HomeScope
import com.example.myapplication.ui.favorite.FavoriteFragment
import com.example.myapplication.ui.home.HomeFragment
import dagger.Component


@HomeScope
@Component(
    dependencies = [AppComponent::class], modules = [MainViewModelFactoryModule::class]
)
interface HomeFragmentComponent {

    fun inject(homeFragment: HomeFragment)

    fun inject(favoriteFragment: FavoriteFragment)

}