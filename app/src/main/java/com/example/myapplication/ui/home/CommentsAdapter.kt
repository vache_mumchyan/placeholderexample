package com.example.myapplication.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.entity.Comment

class CommentsAdapter(private val context: Context, private val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<CommentsAdapter.CommentViewHolder>() {

    private var  comments = ArrayList<Comment>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.comment_item, parent, false)

        return CommentViewHolder(view)
    }

    override fun getItemCount(): Int = comments.size

    override fun getItemId(position: Int) = position.toLong()

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(comments[position])
    }

    fun setData(list : List<Comment>) {
        comments.clear()
        comments.addAll(list)
        notifyDataSetChanged()
    }

    fun deleteItem(comment: Comment) {
        for(i in comments.indices) {
            if (comments[i].id == comment.id) {
                comments.removeAt(i)
                notifyItemRemoved(i)
                return
            }
        }
    }

    inner class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvName: TextView = itemView.findViewById(R.id.tvName)
        private val tvEmail: TextView = itemView.findViewById(R.id.tvEmail)
        private val imgFavorite: ImageView = itemView.findViewById(R.id.imgFavorite)

        fun bind(comment: Comment) {
            tvName.text = comment.name
            tvEmail.text = comment.email

            if (comment.isFavorite){
                imgFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_fill_favorite))
            }else{
                imgFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_empty_favorite))
            }

            imgFavorite.setOnClickListener {
                comment.isFavorite = !comment.isFavorite
                notifyItemChanged(adapterPosition)
                onItemClickListener.onItemClick(comments[adapterPosition])
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(comment: Comment)
    }

}