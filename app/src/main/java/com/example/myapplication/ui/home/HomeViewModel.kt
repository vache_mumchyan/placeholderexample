package com.example.myapplication.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.data.entity.Comment
import com.example.myapplication.data.repository.local.CommentDao
import com.example.myapplication.data.repository.local.LocalRepository
import com.example.myapplication.data.repository.remote.RemoteRepository
import com.example.myapplication.di.scope.HomeScope
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HomeScope
class HomeViewModel @Inject constructor(
    private val repository: RemoteRepository,
    private val localRepository: LocalRepository
) : ViewModel() {

    var data = MutableLiveData<List<Comment>>()
    private val compositeDisposable = CompositeDisposable()

    fun init() {
        Single.zip(
            repository.getComments(),
            localRepository.getAll(),
            BiFunction { remoteData: List<Comment>, localData: List<Comment> ->
                val localMap = linkedMapOf<Int, Comment>()

                localData.forEach {
                    localMap[it.id] = it
                }

                for (i in remoteData.indices) {
                    localMap[remoteData[i].id]?.let {
                        remoteData.toMutableList()[i].isFavorite = true
                    }
                }

                remoteData
            })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                data.value = it
            }, {

            })
            .let {
                compositeDisposable.add(it)
            }
    }

    fun onFavoriteAction(comment: Comment) {
        localRepository.insert(comment)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {}
            .let {
                compositeDisposable.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}