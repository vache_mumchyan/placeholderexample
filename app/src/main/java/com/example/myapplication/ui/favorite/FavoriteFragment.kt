package com.example.myapplication.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.App
import com.example.myapplication.R
import com.example.myapplication.data.entity.Comment
import com.example.myapplication.di.FavoriteViewModelProviderFactory
import com.example.myapplication.di.component.DaggerHomeFragmentComponent
import com.example.myapplication.ui.home.CommentsAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject


class FavoriteFragment : Fragment(), CommentsAdapter.OnItemClickListener {

    @Inject
    lateinit var favoriteViewModelProvider: FavoriteViewModelProviderFactory
    private lateinit var viewModel: FavoriteViewModel
    private lateinit var commentsAdapter: CommentsAdapter

    companion object {
        fun newsInstance(): Fragment {
            return FavoriteFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerHomeFragmentComponent.builder()
            .appComponent((activity?.applicationContext as App).appComponent)
            .build()
            .inject(this)

        viewModel = ViewModelProvider(this, favoriteViewModelProvider)[FavoriteViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.init()
        recycler_comments.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        commentsAdapter = CommentsAdapter(this.context!!, this)

        recycler_comments.adapter = commentsAdapter
        recycler_comments.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )

        viewModel.data.observe(viewLifecycleOwner, Observer {
            commentsAdapter.setData(it)
        })

        viewModel.deleteItem.observe(viewLifecycleOwner, Observer {
            commentsAdapter.deleteItem(it)
        })
    }

    override fun onItemClick(comment: Comment) {
        viewModel.onFavoriteAction(comment)
    }

}
