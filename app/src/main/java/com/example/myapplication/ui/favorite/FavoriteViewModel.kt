package com.example.myapplication.ui.favorite

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.data.entity.Comment
import com.example.myapplication.data.repository.local.LocalRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FavoriteViewModel@Inject constructor(
    private val localRepository: LocalRepository
) : ViewModel() {

    var data = MutableLiveData<List<Comment>>()
    var deleteItem = MutableLiveData<Comment>()
    private val compositeDisposable = CompositeDisposable()

    fun init() {
        localRepository.getAll()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                data.value = it
            }, {

            })
            .let {
                compositeDisposable.add(it)
            }
    }

    fun onFavoriteAction(comment: Comment) {
        localRepository.delete(comment)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                deleteItem.value = comment
            }
            .let {
                compositeDisposable.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}